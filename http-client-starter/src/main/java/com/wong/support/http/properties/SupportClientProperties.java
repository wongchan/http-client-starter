package com.wong.support.http.properties;

import com.wong.support.http.LoadBalanceEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * ClassName SupportClientProperties
 * Author chenwang
 * Date 2021/9/24 9:56
 * Description
 * Version 1.0
 */
@Data
@ConfigurationProperties(prefix = "support.client")
public class SupportClientProperties {

    private LoadBalanceEnum strategy = LoadBalanceEnum.POLLING;

    private Integer connectTimeout = 1000;

    private Integer requestTimeout = 3000;
}
