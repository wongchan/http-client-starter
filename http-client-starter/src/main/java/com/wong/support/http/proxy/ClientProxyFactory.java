package com.wong.support.http.proxy;

import com.wong.support.http.Configuration;
import com.wong.support.http.bean.NacosClient;
import org.springframework.beans.factory.BeanFactory;

import java.lang.reflect.Proxy;

/**
 * ClassName ClientProxyFactory
 * Author chenwang
 * Date 2021/9/17 11:12
 * Description client代理工厂类
 * 使用jdk动态代理，返回了一个client代理后的
 * 代理对象ClientProxy，可以对其进行各种增强操作。
 * Version 1.0
 */
public class ClientProxyFactory<T> {

    private final Class<T> clientInterface;

    public ClientProxyFactory(Class<T> clientInterface) {
        this.clientInterface = clientInterface;
    }

    @SuppressWarnings("unchecked")
    protected T newInstance(ClientProxy<T> clientProxy) {
        return (T) Proxy.newProxyInstance(this.clientInterface.getClassLoader(), new Class[]{this.clientInterface}, clientProxy);
    }

    public T newInstance(Configuration configuration) {
        ClientProxy<T> clientProxy = new ClientProxy<>(this.clientInterface,configuration);
        return this.newInstance(clientProxy);
    }
}
