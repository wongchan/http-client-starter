package com.wong.support.http.exception;

/**
 * ClassName BindException
 * Author chenwang
 * Date 2021/9/17 11:15
 * Description
 * Version 1.0
 */
public class BindException extends RuntimeException{

    private static final long serialVersionUID = -3660712990593812578L;

    public BindException(String message) {
        super(message);
    }

    public BindException(String message, Throwable cause) {
        super(message, cause);
    }
}
