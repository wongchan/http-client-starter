package com.wong.support.http.bean;


import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {

    private final static Long SerializableId = 1L;

    private T data;

    private Integer code;

    private String msg;

    public Result(T data,Integer code,String msg){
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public static Result<String> ok(){
        return new Result<>("",200,"成功");
    }

    public static Result ok(Object data){
        return new Result<>(data,200,"成功");
    }

    public static Result<String> fail(String msg){
        return new Result<>("",500,msg);
    }

    public static Result<String> fail(Integer code,String msg){
        return new Result<>("",code,msg);
    }

    public static Result<String> fail(){
        return new Result<>("",500,"失败");
    }

}
