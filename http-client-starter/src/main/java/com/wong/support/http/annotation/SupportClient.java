package com.wong.support.http.annotation;

import java.lang.annotation.*;

/**
 * ClassName SupportClient
 * Author chenwang
 * Date 2021/9/17 15:23
 * Description http请求客户端注解类
 * Version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SupportClient {

    /**
     * http请求的路径前缀
     * @return
     */
    String url() default "";

    /**
     * 请求服务的application-name(根据name找到nacos配置的服务host以及port)
     * @return
     */
    String name() default "";

    /**
     * ioc容器的bean name
     * @return
     */
    String beanId() default "";

    /**
     * application的访问前缀
     * @return
     */
    String contextPath() default "";
}
