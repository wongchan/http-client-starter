package com.wong.support.http;

/**
 * ClassName LoadBalanceEnum
 * Author chenwang
 * Date 2021/9/24 9:58
 * Description
 * Version 1.0
 */
public enum LoadBalanceEnum {

    /**
     * 随机
     */
    RANDOM,
    /**
     * 轮询
     */
    POLLING
}
