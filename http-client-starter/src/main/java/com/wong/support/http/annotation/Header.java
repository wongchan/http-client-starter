package com.wong.support.http.annotation;


import java.lang.annotation.*;

/**
 * ClassName Header
 * Author chenwang
 * Date 2021/9/17 15:23
 * Description add a header for http request
 * Version 1.0
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Header {

    /**
     * header name
     * @return
     */
    String value() default "";

}
