package com.wong.support.http.annotation;


import java.lang.annotation.*;

/**
 * 文件上传用
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestPart {

    String value() default "";
}
