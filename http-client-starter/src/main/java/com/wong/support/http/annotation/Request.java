package com.wong.support.http.annotation;


import com.wong.support.http.HttpMethod;

import java.lang.annotation.*;

/**
 * ClassName Request
 * Author chenwang
 * Date 2021/9/17 15:23
 * Description
 * Version 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Request {


    /**
     * 请求路径
     * @return
     */
    String value() default "";

    /**
     * 请求类型(默认get)
     * @return
     */
    HttpMethod type() default HttpMethod.GET;

    /**
     * 数据内容类型
     * @return
     */
    String contentType() default "";
}
