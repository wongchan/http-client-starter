package com.wong.support.http.proxy;

import com.wong.support.http.Configuration;
import com.wong.support.http.exception.BindException;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName ClientRegistry
 * Author chenwang
 * Date 2021/9/17 11:11
 * Description client注册
 * 实现每个client的存放以及获取
 * Version 1.0
 */
public class ClientRegistry {

    /**
     * 存放clientProxyFactory
     */
    private final Map<Class<?>, ClientProxyFactory<?>> knownClients = new HashMap<>();

    private volatile static ClientRegistry registry;

    /**
     * 获取ClientRegistry的实例对象
     * 使用了单例模式创建，存放了所有的client注册信息
     * 双重检查保证创建一次对象。
     * @return 返回创建的 ClientRegistry
     */
    public static ClientRegistry getInstance(){
        if(registry != null){
            return registry;
        }
        synchronized (ClientRegistry.class){
            if(registry == null){
                registry = new ClientRegistry();
            }
            return registry;
        }
    }


    private ClientRegistry() {
    }

    /**
     * 获取client对象
     * 将nacosClient信息作为入参，得到一个type类型的client实例对象
     * 但是返回的是一个代理对象。
     * @param type 类对象
     * @param configuration client配置
     * @return client对象
     */
    public <T> T getClient(Class<T> type, Configuration configuration) {
        final ClientProxyFactory<T> clientProxyFactory = (ClientProxyFactory)this.knownClients.get(type);
        if (clientProxyFactory == null) {
            throw new BindException("类： " + type + "未发现注册在 ClientRegistry");
        } else {
            try {
                return clientProxyFactory.newInstance(configuration);
            } catch (Exception var5) {
                throw new BindException("获取client实例错误. Cause: " + var5, var5);
            }
        }
    }


    /**
     * 判断一个class类型是否已经存放在map集合中
     * @param type 类对象
     * @return true/false
     */
    public <T> boolean hasClient(Class<T> type) {
        return this.knownClients.containsKey(type);
    }


    /**
     * 将每个需要的client的Class类信息已经Factory实例加入到map中存放（knownClients）
     * @param type 类对象
     */
    public <T> void addClient(Class<T> type) {
        if (type.isInterface()) {
            if (this.hasClient(type)) {
                throw new BindException("类： " + type + "已经注册在 ClientRegistry.");
            }

            boolean loadCompleted = false;

            try {
                this.knownClients.put(type, new ClientProxyFactory<>(type));
                loadCompleted = true;
            } finally {
                if (!loadCompleted) {
                    this.knownClients.remove(type);
                }
            }
        }
    }
}
