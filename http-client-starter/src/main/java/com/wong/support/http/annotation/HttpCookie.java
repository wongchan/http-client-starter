package com.wong.support.http.annotation;


import java.lang.annotation.*;

/**
 * ClassName HttpCookie
 * Author chenwang
 * Date 2021/9/17 15:23
 * Description: http cookies has a map
 * Version 1.0
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HttpCookie {
   /**
    * cookie of attribute name
    * @return
    */
   String name() default "";

}
