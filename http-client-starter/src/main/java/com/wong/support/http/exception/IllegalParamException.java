package com.wong.support.http.exception;

/**
 * ClassName IllegalParamException
 * Author chenwang
 * Date 2021/9/22 16:10
 * Description
 * Version 1.0
 */
public class IllegalParamException extends RuntimeException{


    private static final long serialVersionUID = 7427115674523922934L;

    public IllegalParamException(String message) {
        super(message);
    }

    public IllegalParamException(String message, Throwable cause) {
        super(message, cause);
    }
}
