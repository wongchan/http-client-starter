package com.wong.support.http;

import lombok.Data;
import org.springframework.beans.factory.BeanFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName ClientConfig
 * Author chenwang
 * Date 2021/9/22 16:35
 * Description
 * Version 1.0
 */
@Data
public class Configuration {


    private Map<String,Handler> handlerMap;

    /**
     * beanFactory
     */
    private BeanFactory beanFactory;

    /**
     * 二级路径
     */
    private String url;

    /**
     * 服务名称（注册在nacos上的application.name）
     */
    private String name;

    /**
     * 路径前缀
     */
    private String contextPath;

    /**
     * 域名(http://host:port)
     */
    private String domain;
}
