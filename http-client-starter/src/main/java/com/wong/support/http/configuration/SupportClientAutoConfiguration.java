package com.wong.support.http.configuration;

import com.wong.support.http.properties.SupportClientProperties;
import com.wong.support.http.properties.SupportGlobalConfigProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * ClassName SupportClientAutoConfiguration
 * Author chenwang
 * Date 2021/9/24 9:52
 * Description
 * Version 1.0
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties({SupportGlobalConfigProperties.class,SupportClientProperties.class})
public class SupportClientAutoConfiguration {

}
