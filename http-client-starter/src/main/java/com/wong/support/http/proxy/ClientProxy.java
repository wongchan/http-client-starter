package com.wong.support.http.proxy;

import com.wong.support.http.Configuration;
import com.wong.support.http.Context;
import com.wong.support.http.LoadBalanceEnum;
import com.wong.support.http.bean.NacosClient;
import com.wong.support.http.exception.IllegalParamException;
import com.wong.support.http.properties.SupportClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ClassName ClientProxy
 * Author chenwang
 * Date 2021/9/17 10:59
 * Description client代理实现类
 * Version 1.0
 */
@Slf4j
public class ClientProxy<T> implements InvocationHandler, Serializable {


    private static final long serialVersionUID = 8057508454903201403L;

    private final Class<T> clientInterface;

    private final Configuration configuration;

    private int times = 0;

    public ClientProxy(Class<T> clientInterface, Configuration configuration) {
        this.clientInterface= clientInterface;
        this.configuration = configuration;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        initDomain();
        return Context.handle(method,args,configuration);
    }

    private void initDomain(){
        if(!StringUtils.hasText(configuration.getName()) && StringUtils.hasText(configuration.getUrl())){
            configuration.setDomain(configuration.getUrl());
            return;
        }
        NacosClient nacosClient = getNacosClient();
        List<ServiceInstance> serviceInstances = nacosClient.getInstanceMap().get(configuration.getName());
        if(CollectionUtils.isEmpty(serviceInstances)){
            log.error("注册中心上找不到名为 {}的服务！",configuration.getName());
            throw new IllegalParamException("service instances not find!");
        }
        LoadBalanceEnum strategy = configuration.getBeanFactory().getBean(SupportClientProperties.class).getStrategy();
        int n;
        switch (strategy){
            case POLLING:
                Lock lock = new ReentrantLock();
                lock.lock();
                try {
                    n = times % serviceInstances.size();
                    times ++ ;
                }catch (Exception e){
                    log.error("轮询算法错误，"+e.getMessage());
                    n = 0;
                }finally {
                    lock.unlock();
                }
                break;
            default:
            case RANDOM:
                n = (int) (Math.random()*serviceInstances.size()*10)%serviceInstances.size();
                break;
        }
        ServiceInstance serviceInstance = serviceInstances.get(n);
        configuration.setDomain("http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort());
    }



    private NacosClient getNacosClient(){
        NacosClient nacosClient = new NacosClient();
        DiscoveryClient client = configuration.getBeanFactory().getBean(DiscoveryClient.class);
        List<String> services = client.getServices();
        if(CollectionUtils.isEmpty(services)){
            return nacosClient;
        }
        Map<String, List<ServiceInstance>> map = new HashMap<>(16);
        for (String service : services) {
            List<ServiceInstance> instances = client.getInstances(service);
            if(CollectionUtils.isEmpty(instances)){
                continue;
            }
            instances.removeIf(e->{
                Map<String, String> metadata = e.getMetadata();
                if(metadata == null){
                    return false;
                }
                return "false".equals(metadata.get("nacos.healthy"));
            });
            map.put(service,instances);
        }
        nacosClient.setInstanceMap(map);
        nacosClient.setServices(services);
        return nacosClient;
    }
}
