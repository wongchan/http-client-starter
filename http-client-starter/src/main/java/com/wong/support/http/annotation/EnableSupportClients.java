package com.wong.support.http.annotation;

import com.wong.support.http.SupportClientsRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * ClassName EnableSupportClients
 * Author chenwang
 * Date 2021/9/17 15:10
 * Description
 * Version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(SupportClientsRegistrar.class)
public @interface EnableSupportClients {

    /**
     * client包扫描根路径
     * @return
     */
    String[] basePackages() default {};
}
