package com.wong.support.http;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.wong.support.http.annotation.*;
import com.wong.support.http.properties.SupportClientProperties;
import com.wong.support.http.exception.IllegalParamException;
import com.wong.support.http.properties.SupportGlobalConfigProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.core.io.InputStreamSource;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URI;
import java.util.*;

/**
 * ClassName Context
 * Author chenwang
 * Date 2021/9/24 9:40
 * Description
 * Version 1.0
 */
@Slf4j
public class Context {

    public static Object handle(Method method, Object[] args,Configuration configuration){
        //创建http客户端对象
        HttpClient client = HttpClientBuilder.create().build();
        SupportGlobalConfigProperties globalProperties = configuration.getBeanFactory().getBean(SupportGlobalConfigProperties.class);
        if(globalProperties.getLogPrint()){
            log.info("=============================================support请求开始=========================================================");
        }
        SupportClientProperties properties = configuration.getBeanFactory().getBean(SupportClientProperties.class);
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(properties.getConnectTimeout())
                .setConnectionRequestTimeout(properties.getRequestTimeout())
                .build();
        Map<String, Handler> handlerMap = configuration.getHandlerMap();
        Handler handler = handlerMap.get(method.getName());
        if(handler == null){
            throw new IllegalParamException("方法映射失败，请检查：method name:"+method.getName());
        }
        String domain = configuration.getDomain();
        Parameter[] parameters = method.getParameters();
        StringBuilder url = new StringBuilder(domain+configuration.getContextPath()+handler.getUri());
        HttpRequestBase request;
        switch (handler.getMethod()){
            default:
            case GET:
                request = get(parameters,args,url);
                break;
            case POST:
                request = post(parameters,args,url);
                break;
        }
        //content-type自定义
        if(!ObjectUtils.isEmpty(handler.getContentType())){
            request.setHeader("Content-Type",handler.getContentType());
        }
        request.setConfig(config);
        //默认的header加入。
        globalProperties.getGlobalHeader().forEach(request::addHeader);
        //默认的cookie加入
        StringBuilder builder = new StringBuilder();
        globalProperties.getGlobalCookies().forEach((k, v) -> builder.append(k).append("=").append(v).append(";"));
        org.apache.http.Header cookie = Arrays.stream(request.getAllHeaders()).filter(e -> e.getName().equals("Cookie")).findFirst().orElse(null);
        if (cookie == null) {
            request.addHeader("Cookie", builder.toString());
        } else {
            request.setHeader("Cookie", cookie.getValue() + ";" + builder.toString());
        }
        try {
            long l = System.currentTimeMillis();
            HttpResponse execute = client.execute(request);
            HttpEntity entity = execute.getEntity();
            String result = EntityUtils.toString(entity);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                // 返回json格式
                if(globalProperties.getLogPrint()) {
                    log.info("请求方式：{}",request.getMethod());
                    log.info("请求路径：{}",request.getURI());
                    log.info("请求HEADER：{}", new ArrayList<>(Arrays.asList(request.getAllHeaders())).toString());
                    log.info("返回结果：{}",result);
                    log.info("响应时间：{}ms",System.currentTimeMillis() - l);
                }
                if(checkBasicType(method.getReturnType())){
                    return result;
                }
                Object object;
                try{
                    object = JSONObject.parseObject(result, method.getReturnType());
                }catch (JSONException e){
                    log.error("返回结果 json 解析异常：should ReturnType:{}",method.getReturnType());
                    return null;
                }
                return object;
            } else {
                log.warn("support http 请求响应状态码异常,请求路径：{}，状态码：{},相关信息：{}",request.getURI(), statusCode,execute.getStatusLine().getReasonPhrase());
                return null;
            }
        } catch (IOException e) {
            log.error("请求/响应失败 msg:{}",e.getMessage());
            e.printStackTrace();
            return null;
        }finally {
            HttpClientUtils.closeQuietly(client);
        }
    }


    public static HttpRequestBase post(Parameter[] parameters, Object[] args, StringBuilder url) {
        HttpPost post = new HttpPost();
        Map<String,String> header = new HashMap<>();
        boolean hasBody = false;
		boolean hasParam = false;
        boolean hasPathVariable = false;
        for (int i = 0; i < parameters.length; i++) {
            if(parameters[i].isAnnotationPresent(Header.class)){
                Header annotation = parameters[i].getAnnotation(Header.class);
                String headerName = annotation.value();
                if(!StringUtils.hasText(headerName)){
                    headerName = parameters[i].getName();
                }
                header.put(headerName, (String) args[i]);
            }else if (parameters[i].isAnnotationPresent(RequestBody.class)){
                checkHasBody(hasBody,parameters[i].getName());
                if(checkBasicType(parameters[i].getType())){
                    throw new IllegalParamException("参数错误，参数对应注解不匹配，参数类型："+args[i].getClass());
                }
                StringEntity s = new StringEntity(JSONObject.toJSONString(args[i]), "UTF-8");
                s.setContentType("application/json");
                post.setEntity(s);
                hasBody = true;
            }else if (parameters[i].isAnnotationPresent(Param.class)){
				if(!hasParam){
					url.append("?");
					hasParam = true;
				}
                checkHasBody(hasBody,parameters[i].getName());
                if(!checkBasicType(parameters[i].getType())){
                    throw new IllegalParamException("参数错误，参数对应注解不匹配，参数类型："+args[i].getClass());
                }
                url.append(parameters[i].getName()).append("=");
                if (parameters[i] != null) {
                    url.append(args[i]);
                }
                url.append("&");
            } else if (parameters[i].isAnnotationPresent(HttpCookie.class)){
                HttpCookie annotation = parameters[i].getAnnotation(HttpCookie.class);
                String cookieName = annotation.name();
                if(!StringUtils.hasText(cookieName)){
                    cookieName = parameters[i].getName();
                }
                if(header.containsKey("Cookie")){
                    header.put("Cookie",header.get("Cookie") + ";" + cookieName + "=" + args[i]);
                }else {
                    header.put("Cookie",cookieName + "=" + args[i]);
                }
            }else if (parameters[i].isAnnotationPresent(RequestPart.class)) {
                if(! ((args[i] instanceof InputStreamSource[]) || (args[i] instanceof InputStreamSource))) {
                    throw new IllegalParamException("请求文件类型不正确，请设置MultipartFile类型");
                }
                InputStreamSource[] arg = new InputStreamSource[1];
                if(args[i] instanceof InputStreamSource[]) {
                    arg = (InputStreamSource[]) args[i];
                } else arg[0] = (InputStreamSource) args[i];

                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                for (InputStreamSource file : arg) {
                    try {
                        Method getName = file.getClass().getMethod("getOriginalFilename");
                        getName.setAccessible(true);
                        String fileName = (String) getName.invoke(file);
                        String name = parameters[i].getAnnotation(RequestPart.class).value();
                        if(ObjectUtils.isEmpty(name)) {
                            name = parameters[i].getName();
                        }
                        builder.addBinaryBody(name, file.getInputStream(), ContentType.MULTIPART_FORM_DATA,fileName);
                    }catch (Exception e){
                        log.error(e.getMessage());
                        throw new IllegalParamException("文件请求异常....");
                    }
                }
                post.setEntity(builder.build());
            }
        }
        header.forEach(post::addHeader);
        post.setURI(URI.create(url.toString()));
        log.info("POST 请求地址：{},请求体：{}",post.getURI(),post.getEntity().toString());
        return post;
    }

    public static HttpRequestBase get(Parameter[] parameters,Object[] args,StringBuilder url) {
        HttpGet get = new HttpGet();
        Map<String,String> header = new HashMap<>();
        boolean hasBody = false;
        boolean hasPathVariable = false;
        boolean hasParam = false;
        for (int i = 0; i < parameters.length; i++) {
            if(parameters[i].isAnnotationPresent(Header.class)){
                Header annotation = parameters[i].getAnnotation(Header.class);
                String headerName = annotation.value();
                if(!StringUtils.hasText(headerName)){
                    headerName = parameters[i].getName();
                }
                header.put(headerName, (String) args[i]);
            }else if (parameters[i].isAnnotationPresent(RequestBody.class)){
                checkHasBody(hasBody,parameters[i].getName());
                if(checkBasicType(parameters[i].getType())){
                    throw new IllegalParamException("参数错误，参数对应注解不匹配，参数类型："+args[i].getClass());
                }
                List<Field> fields = new ArrayList<>(Arrays.asList(parameters[i].getType().getDeclaredFields()));
                fields.addAll(Arrays.asList(parameters[i].getType().getSuperclass().getDeclaredFields()));
                for (Field field : fields) {
                    field.setAccessible(true);
                    try {
                        Object o = field.get(args[i]);
                        if(null == o){
                            continue;
                        }
                        url.append(field.getName()).append("=").append(o).append("&");
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                hasBody = true;
            }else if (parameters[i].isAnnotationPresent(Param.class)){
                checkHasBody(hasBody,parameters[i].getName());
                if(hasPathVariable){
                    throw new IllegalParamException("参数错误，@PathVariable不应该和@Patam一起使用！param："+parameters[i].getName());
                }
                if(!checkBasicType(parameters[i].getType())){
                    throw new IllegalParamException("参数错误，参数对应注解不匹配，参数类型："+args[i].getClass());
                }
                if(args[i] == null){
                    continue;
                }
                if(!hasParam){
                    url.append("?");
					hasParam = true;
                }
                url.append(parameters[i].getName()).append("=").append(args[i]).append("&");
            }else if (parameters[i].isAnnotationPresent(HttpCookie.class)){
                HttpCookie annotation = parameters[i].getAnnotation(HttpCookie.class);
                String cookieName = annotation.name();
                if(!StringUtils.hasText(cookieName)){
                    cookieName = parameters[i].getName();
                }
                if(header.containsKey("Cookie")){
                    header.put("Cookie",header.get("Cookie") + ";" + cookieName + "=" + args[i]);
                }else {
                    header.put("Cookie",cookieName + "=" + args[i]);
                }
            }else if(parameters[i].isAnnotationPresent(PathVariable.class)){
                //有param参数,直接报错
                if(hasParam){
                    throw new IllegalParamException("参数错误，@PathVariable不应该和@Patam一起使用！，param:"+parameters[i].getName());
                }
				if(hasPathVariable){
                    throw new IllegalParamException("参数错误，@PathVariable应该只有一个！，param:"+parameters[i].getName());
                }
				if(!checkBasicType(parameters[i].getType())){
                    throw new IllegalParamException("参数错误，@PathVariable修饰的参数只能是基本数据类型或String类型！，param:"+parameters[i].getName());
                }
                PathVariable pathVariable = parameters[i].getAnnotation(PathVariable.class);
                String value = pathVariable.value();
                if(!StringUtils.hasText(value)){
                    throw new IllegalParamException("参数错误，@PathVariable应该有value属性，但是没有发现，param:"+parameters[i].getName());
                }
                //找到路径上的占位符{}
                int index = url.indexOf("{" + value + "}");
                if(index == -1){
                    throw new IllegalParamException("参数错误，@PathVariable的value值，在url:"+url+" 中找不到映射。param:"+parameters[i].getName());
                }
                //占位符写法
                url.replace(index,index+value.length()+2, args[i].toString());
                hasPathVariable = true;
            }
        }
        header.forEach(get::addHeader);
        get.setURI(URI.create(url.toString()));
        log.info("GET 请求地址：{}",get.getURI());
        return get;
    }


    private static void checkHasBody(Boolean hasBody,String name){
        if(hasBody){
            throw new IllegalParamException("请求参数不符合规范！参数名："+name);
        }
    }

    public static Boolean checkBasicType(Class clazz){

        if (clazz.getSimpleName().equals("String")) {
            return true;
        } else if (clazz.getSimpleName().equals("Integer")) {
            return true;
        } else if (clazz.getSimpleName().equals("Long")) {
            return true;
        } else if (clazz.getSimpleName().equals("Double")) {
            return true;
        } else if (clazz.getSimpleName().equals("Float")) {
            return true;
        } else {
            return clazz.getSimpleName().equals("Short");
        }
    }

}
