package com.wong.support.http.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName SupportClientProperties
 * Author chenwang
 * Date 2021/9/24 9:56
 * Description
 * Version 1.0
 */
@Data
@ConfigurationProperties(prefix = "support.config")
public class SupportGlobalConfigProperties {


    private Map<String,String> globalHeader = new HashMap<>();

    private Map<String,String> globalCookies = new HashMap<>();

    private Boolean logPrint = true;
}
