package com.wong.support.http.bean;

import lombok.Data;
import org.springframework.cloud.client.ServiceInstance;

import java.util.List;
import java.util.Map;

/**
 * ClassName NacosClient
 * Author chenwang
 * Date 2021/9/18 9:51
 * Description
 * Version 1.0
 */
@Data
public class NacosClient {

    private List<String> services;

    private Map<String,List<ServiceInstance>> instanceMap;
}
