package com.wong.support.http;





public enum HttpMethod {

    POST,
    GET,
    DELETE,
    PUT;
}
