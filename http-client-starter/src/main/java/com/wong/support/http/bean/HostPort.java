package com.wong.support.http.bean;

import lombok.Data;

/**
 * ClassName HostPort
 * Author chenwang
 * Date 2021/9/17 17:49
 * Description
 * Version 1.0
 */
@Data
public class HostPort {


    private String host;


    private String port;
}
