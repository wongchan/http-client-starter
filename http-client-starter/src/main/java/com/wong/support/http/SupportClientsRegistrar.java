package com.wong.support.http;

import com.wong.support.http.annotation.EnableSupportClients;
import com.wong.support.http.annotation.Request;
import com.wong.support.http.annotation.SupportClient;
import com.wong.support.http.exception.IllegalParamException;
import com.wong.support.http.proxy.ClientRegistry;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 *  ClassName SupportClientsRegistrar
 *  Author chenwang
 *  2021/9/17 15:27
 *  client注册类
 * 实现ImportBeanDefinitionRegistrar并重写其registerBeanDefinitions方法
 * 自定义bean的定义以及加入ioc容器，实现ResourceLoaderAware和EnvironmentAware
 * 获取当前application的一些环境信息，便于扫描需要注入的拥有@SupportCient注解的接口
 * 实现了BeanFactoryAware，将方便获取ioc容器已注入的bean提供使用，比如获取注册在
 * nacos中/DiscoveryClient 中的应用的serviceId以及他们的host、port。以便在进行
 * 封装最终http请求时的请求路径。
 */
public class SupportClientsRegistrar implements ImportBeanDefinitionRegistrar, ResourceLoaderAware, EnvironmentAware, BeanFactoryAware {

    private ResourceLoader resourceLoader;

    private Environment environment;

    private BeanFactory beanFactory;

    private final ClientRegistry clientRegistry = ClientRegistry.getInstance();

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }


    /**
     * 重写registerBeanDefinitions方法进行bean的自定义注入实现
     * @param metadata
     * @param registry
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        registerSupportClients(metadata, registry);
    }


    /**
     * 注册所有的client
     *
     * @param metadata
     * @param registry
     */
    public void registerSupportClients(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {

        LinkedHashSet<BeanDefinition> candidateComponents = new LinkedHashSet<>();
        ClassPathScanningCandidateComponentProvider scanner = getScanner();
        scanner.setResourceLoader(this.resourceLoader);
        //扫描的条件，拥有@SupportClient注解的类
        scanner.addIncludeFilter(new AnnotationTypeFilter(SupportClient.class));
        //需要扫描的包路径
        Set<String> basePackages = getBasePackages(metadata);
        for (String basePackage : basePackages) {
            candidateComponents.addAll(scanner.findCandidateComponents(basePackage));
        }
        for (BeanDefinition candidateComponent : candidateComponents) {
            try {
                Class<?> aClass = Class.forName(candidateComponent.getBeanClassName());
                clientRegistry.addClient(aClass);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            AnnotatedBeanDefinition beanDefinition = (AnnotatedBeanDefinition) candidateComponent;
            registerFeignClient(registry, beanDefinition.getMetadata());
        }
    }

    private void registerFeignClient(BeanDefinitionRegistry registry, AnnotationMetadata annotationMetadata) {
        String className = annotationMetadata.getClassName();
        Class clazz = ClassUtils.resolveClassName(className, null);
        Configuration configuration = initConfig(clazz);
        BeanDefinitionBuilder definition = BeanDefinitionBuilder.genericBeanDefinition(clazz, () -> {
            return clientRegistry.getClient(clazz,configuration);
        });
        definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
        definition.setLazyInit(true);

        AbstractBeanDefinition beanDefinition = definition.getBeanDefinition();

        beanDefinition.setPrimary(true);

        Map<String, Object> attributes = annotationMetadata.getAnnotationAttributes(SupportClient.class.getCanonicalName());
        Assert.notNull(attributes,"SupportClient 注解上的属性未找到！相关接口："+clazz.getName());
        String beanId = (String) attributes.get("beanId");
        if (!StringUtils.hasText(beanId)){
            beanId = clazz.getSimpleName();
        }
        registry.registerBeanDefinition(beanId,beanDefinition);
    }


    /**
     * 创建一个ClassPathScanningCandidateComponentProvider的实例，并且
     * 定义了top-level（是否独立，即顶级类）
     * 关于是否是top-level类可参考 java.lang.Class#getEnclosingClass方法
     * @return
     */
    protected ClassPathScanningCandidateComponentProvider getScanner() {
        return new ClassPathScanningCandidateComponentProvider(false, this.environment) {
            @Override
            protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
                boolean isCandidate = false;
                if (beanDefinition.getMetadata().isIndependent()) {
                    if (!beanDefinition.getMetadata().isAnnotation()) {
                        isCandidate = true;
                    }
                }
                return isCandidate;
            }
        };
    }

    /**
     * 初始化配置
     * 读取注解上的属性信息，封装配置类
     * @param clazz
     * @return
     */
    private Configuration initConfig(Class<?> clazz){
        Configuration configuration = new Configuration();
        Map<String,Handler> map = new HashMap<>();
        SupportClient annotation = clazz.getAnnotation(SupportClient.class);
        if(annotation == null){
            throw new IllegalParamException("client do not modification by @SupportClient!");
        }
        //获取所有方法
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            //先判断是否为@Request 注解修饰
            if (!method.isAnnotationPresent(Request.class)){
                continue;
            }
            Request request = method.getAnnotation(Request.class);
            String value = request.value();
            HttpMethod type = request.type();
            String contentType = request.contentType();

            Handler handler = new Handler(type,value);
            handler.setContentType(contentType);
            map.put(method.getName(),handler);
        }
        //name 、url两种方式
        if(!StringUtils.hasText(annotation.name()) && !StringUtils.hasText(annotation.url())){
            throw new IllegalParamException("application name or url must be have one!");
        }
        configuration.setHandlerMap(map);
        configuration.setName(annotation.name());
        configuration.setUrl(annotation.url());
        configuration.setContextPath(annotation.contextPath());
        configuration.setBeanFactory(beanFactory);
        return configuration;
    }

    /**
     * 扫描basePackages
     * 先找到@EnableSupportClients的basePackages属性上的所有的路径
     * 获取所有的包名
     * @param importingClassMetadata
     * @return
     */
    protected Set<String> getBasePackages(AnnotationMetadata importingClassMetadata) {
        Map<String, Object> attributes = importingClassMetadata
                .getAnnotationAttributes(EnableSupportClients.class.getCanonicalName());

        Set<String> basePackages = new HashSet<>();
        for (String pkg : (String[]) attributes.get("basePackages")) {
            if (StringUtils.hasText(pkg)) {
                basePackages.add(pkg);
            }
        }
        if (basePackages.isEmpty()) {
            basePackages.add(ClassUtils.getPackageName(importingClassMetadata.getClassName()));
        }
        return basePackages;
    }

    private String getClientName(Map<String, Object> client) {
        if (client == null) {
            return null;
        }
        String value = (String) client.get("beanId");
        if (!StringUtils.hasText(value)) {
            value = (String) client.get("name");
        }
        if (!StringUtils.hasText(value)) {
            value = (String) client.get("serviceId");
        }
        if (StringUtils.hasText(value)) {
            return value;
        }

        throw new IllegalStateException(
                "Either 'name' or 'value' must be provided in @" + SupportClient.class.getSimpleName());
    }
}
