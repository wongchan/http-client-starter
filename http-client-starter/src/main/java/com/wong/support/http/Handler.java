package com.wong.support.http;

import lombok.Data;

/**
 * ClassName Handler
 * Author chenwang
 * Date 2021/9/18 16:58
 * Description
 * Version 1.0
 */
@Data
public class Handler {

    private HttpMethod method;

    private String uri;

    private Object jsonObject;

    private Class<?> resultType;

    private String contentType;

    public Handler(HttpMethod method, String uri) {
        this.method = method;
        this.uri = uri;
    }
}
