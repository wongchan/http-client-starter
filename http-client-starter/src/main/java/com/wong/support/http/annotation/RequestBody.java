package com.wong.support.http.annotation;


import java.lang.annotation.*;

/**
 * ClassName RequestBody
 * Author chenwang
 * Date 2021/9/17 15:23
 * Description
 * Version 1.0
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestBody {
}
