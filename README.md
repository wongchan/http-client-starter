# http-client-starter

#### 介绍
http微服务远程通信框架

#### 软件架构
软件架构说明

#### 设计思路：

考虑自定义starter，置顶配置类，通过@EnableConfigurationProperties注解配置相关yml自定义配置、

另一方面，定义注解@SupportClients，相当于@FeignClients注解，作用于启动类上，通过该注解 @Import(SupportClientsRegistrar.class)

加载一个实现了ImportBeanDefinitionRegistrar接口的注册类，从而在该类中实现对带有@SupportClient的类 实现和注入。

client需要进行代理，所以在registry中，需要将其通过代理对象注入到容器当中。

而当访问带有@SupportClient接口的方法时，还顺便有@Request、@RequestBody、@Param等注解·，帮助指定请求参数和请求头/请求的请求体。

通过反射进行http请求的参数的绑定和一些设置，最后执行实现了InvocationHandler接口的invoke方法，并且通过json解析返回结果。

#### 安装教程
 maven导入依赖：
        <dependency>
            <groupId>io.gitee.wongchan</groupId>
            <artifactId>http-client-starter</artifactId>
            <version>2.0</version>
        </dependency>

#### 使用说明

依赖于springboot项目，
将@EnableSupportCients注解置于启动类上，并且指定client的包名
将@SupportClient 注解置于client接口上，指明name/url ,contextPath等属性，
并通过@Request、@Param、@RequestBody、@Header等注解进行接口参数的绑定。
在consumer方，使用@Autowired 进行注入，直接调用接口方法即可。

#### 参与贡献

1. wong


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

版本更新说明：
----------------------------------------------------------------------------------------------------------------------
http-client-starter:  2.0.1 
1.修复spring应用启动失败的问题。




http-client-starter 2.0：

1.增加@SupportClient 的url的支持，可以不使用application来进行通信，直接使用url路径

2.增加更多的异常检查和处理

3.提供初始化全局配置，比如日志，全局header设置。
